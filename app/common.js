const Model = require('./knowledge-base/knowledge/knowledge.model');
const lunr = require('lunr');
const config = require('./config/config.json');
exports.read_config = function(){
    // preferred path
    const configFile = path.join(__dirname, 'config', 'config.json');

    // depreciated path
    const defaultConfigFile = path.join(__dirname, 'config.js');
    // if(fs.existsSync(defaultConfigFile) === true){
    //     // create config dir if doesnt exist
    //     const dir = path.join(__dirname, '..', 'config');
    //     if(!fs.existsSync(dir)){
    //         fs.mkdirSync(dir);
    //     }

        // if exists, copy our config from /routes to /config
        // const tempconfig = fs.readFileSync(defaultConfigFile, 'utf8');
        // fs.writeFileSync(configFile, tempconfig, 'utf8');
        // remove old file
        // fs.unlinkSync(defaultConfigFile);
    // }
    // load config file
    const rawData = fs.readFileSync(configFile, 'utf8');
    const loadedConfig = JSON.parse(rawData);

    // if(loadedConfig.settings.database.type === 'mongodb'){
    //     loadedConfig.settings.database.connection_string = process.env.MONGODB_CONNECTION_STRING || loadedConfig.settings.database.connection_string;
    // }

    // if(typeof loadedConfig.settings.route_name === 'undefined' || loadedConfig.settings.route_name === ''){
    //     loadedConfig.settings.route_name = 'kb';
    // }

    // set the environment depending on the NODE_ENV
    let environment = '.min';
    if(process.env.NODE_ENV === 'development' || process.env.NODE_ENV === undefined){
        environment = '';
    }
    loadedConfig.settings.env = environment;

    return loadedConfig;
};

module.exports.buildIndex = async function(callback){
    // const config = this.read_config();
    let kb_list =  await Model.find({ kb_published: 'true' } )
        // build the index
        const index = lunr(function(){
            this.field('kb_title');
            this.field('kb_keywords');
            this.ref('id');

            // add body to this if in config
            if(config.settings.index_article_body === true){
                this.field('kb_body');
            }

        // add to lunr this
        kb_list.forEach((kb) => {
            // only if defined
            let keywords = '';
            if(kb.kb_keywords !== undefined){
                keywords = kb.kb_keywords.toString().replace(/,/g, ' ');
            }

            const doc = {
                kb_title: kb.kb_title,
                kb_keywords: keywords,
                id: kb._id
            };

            // if this body is switched on
            if(config.settings.index_article_body === true){
                doc['kb_body'] = kb.kb_body;
            }
            this.add(doc)
        })
        })
        callback(index);
    }

module.exports.getId = async(id)=>{
    if(config.settings.database.type === 'embedded'){
        return id;
    }
    if(id.length !== 24){
        return id;
    }
    let returnID = '';
    try{
        returnID = ObjectID(id);
        return returnID;
    }catch(ex){
        return id;
    }
};


