let Admin = require('./admin.model');
let User = require('./user.model');
let Agent = require('./agent.model')
const jwt = require('jsonwebtoken');
const moment = require('moment');
const mongoose = require('mongoose');
// const {sendEmail} = require('../../config/sender')
require('dotenv').config();
function validationError(res, statusCode) {
    statusCode = statusCode || 422;
    return function (err) {
        return res.status(statusCode).json(err);
    };
}

let students = [
    {
        "name":"rohit kumar",
        "enroll_no":"9897",
        "roll_no":"15-2015",
        "course":"B.TECH",
        "email":"rohitanp@gmail.com",
        "contactNumber":"9897564110"
    },
    {
        "name":"amit kumar",
        "enroll_no":"9898",
        "roll_no":"15-2016",
        "course":"B.TECH",
        "email":"amitanp@gmail.com",
        "contactNumber":"9897564111"
    },
    {
        "name":"atul kumar",
        "enroll_no":"9899",
        "roll_no":"15-2017",
        "course":"B.TECH",
        "email":"atuldutta@gmail.com",
        "contactNumber":"9897564112"
    },
    {
        "name":"afzal ahmad",
        "enroll_no":"9901",
        "roll_no":"15-2018",
        "course":"M.C.A",
        "email":"afzal@gmail.com",
        "contactNumber":"9897564113"
    }

]
module.exports.getStudents = async(req,res)=>{
    let filter = {};
    let filtered;
    if(req.query.name){
        filtered = students.filter(o=> o.name == req.query.name)
    }

    else if(req.query.id){
        filtered = students.filter(o=> o.id == req.query.id)
    }

    else if(req.query.enroll_no){
        filtered = students.filter(o => o.enroll_no == req.query.enroll_no);
    }
    else if(req.query.roll_no){
        filtered = students.filter(o=> o.roll_no == req.query.roll_no);
        console.log(filtered)
    }

    else if(req.query.course){
        filtered = students.filter(o=> o.course == req.query.course);

    }
    else{
        filtered = students;
    }

    
    res.send({success:true,data:filtered})

}

module.exports.deleteStudent = async(req,res)=>{
    try{
        let enroll_no = req.query.enroll_no;
        const filteredPeople = students.filter((item) => item.enroll_no !== enroll_no);
        res.send({success:true,data:filteredPeople})
    }catch(error){
        res.status(400).send({success:false,message:error})
    }
}
module.exports.createUser = async(req,res)=>{
    if(req.body.role=='Admin'){
        var newUser = new Admin(req.body);
    }
    else{
        var newUser = new Agent(req.body);
    }
    newUser.provider = 'local';
    newUser.username = req.body.email;
    if(!req.body.role){
        req.body.role = 'User';
    }
    // let otp = Math.floor((Math.random() * 1000000) + 54);
    // newUser.otp = otp;
    // newUser.role = req.body.role;
    // // let mail = await sendEmail(req.body.email,otp);
    // if(mail == -1){
    //     return res.status(400).send({success:false,message:"Either email is not correct or network issue"});
    // }
    newUser.otpTime = new Date();
    await newUser
        .save()
        .then(function (user) {
            var token = jwt.sign({ _id: user._id }, process.env.secret, {
                expiresIn: 60 * 60 * 5,
            });
            // Mails.welcomeMail(user.name, user.email).then(function (d) { });
            return res.status(200).send({
                _id: user._id,
                message: 'Account created successfully.',
            }); 
        })
        .catch(validationError(res));
}
// module.exports.verifiedUser = async(req,res)=>{
//     let pendingUser = await User.findById(req.params.id);
//     let otpTime = pendingUser.otpTime;
//     let expand = moment(otpTime).add(20,'m').toDate();
//     if(expand<new Date()){
//         return res.status(404).send({success:false,message:"otp time expired"})
//     }

//     if(String(pendingUser.otp) != String(req.query.otp)){
//         return res.status(402).send({success:false,message:"otp is wrong"})
//     }

//     await User.findByIdAndUpdate(req.params.id,{activationStatus:true},{new:true})
//     .then(result=>{
//         res.send({
//         success:true,
//         id:pendingUser._id,
//         message:"successfully verified"
// })})
// }
module.exports.getLoggedUser = async(req,res)=>{
    console.log(req.user._id,"id")
    await User.findById(req.user._id).select('-salt -password -__v -provider')
    .then(data=>res.send(data))
    .catch(err=>res.status(400).send(err));

}

module.exports.updateUser = async(req,res)=>{
    try{
        if(req.body.password==""){
            delete req.body.password
        }
        console.log(req.body,"body")
        if(req.body.subRole){
            req.body.subRole =mongoose.Types.ObjectId(req.body.subRole);
            await Editor.findByIdAndUpdate(req.params.id,req.body,{
                new:true
            })
            .then(data=>{
                return res.send({success:true,msg:"update successfully"})
            })
        }
        else{
                await User.findByIdAndUpdate(req.params.id,req.body,{
                new:true
            })
            .then(data=>{
                res.send({success:true,msg:"update successfully"})
            })
        }
    }catch(error){
        res.status(400).send({error:error});
    }
}

module.exports.updatePassword = async(req,res)=>{
    try{
        let user = await User.findById(req.params.id);
        user.password = req.body.password;
        await user.save()
        .then(response=>res.send({success:true,msg:"successfully updated password"}))
        .catch(error=>res.status(404).send({success:false,msg:"something wrong while updating passowrd"}))
    }catch(error){
        res.status(400).send({error:error});
    }
}

module.exports.getAllUser = async(req,res)=>{
    const page = parseInt(req.query.page) || 1;
    let limit = parseInt(req.query.limit) || 10;
    const skip = (page - 1) * limit;
    //because admin skipped in user list
    if(page === 1){
        limit += 1;
    }
    try{
        if(req.query.role){
            var filter = {
                role:req.query.role,
            }
        }

        await User.find(filter).select('-salt -password -__v -provider').skip(skip).limit(limit).then(data=>
            {
                if(req.query.role){
                    return res.send(data);
                }
                return res.send(data.slice(1));
            })
            .catch(error=>res.status(400).send(error));

    }catch(error){
        res.status(400).send({error:error});
    }
}

module.exports.deleteUser = async(req,res)=>{
    try{
        let article = await Article.find({creatorId:mongoose.Types.ObjectId(req.params.id),readyToPublish:true});
        if(article.length!=0){
            return res.status(403).send({msg:"this user have article's(without published)"});
        }
        await User.findByIdAndDelete(req.params.id)
        .then(data=>{
            res.send({success:true,msg:"successfully delete"})
        }).catch(error=>res.status(400).send({error:error}))
    }catch(error){
        res.status(400).send({error:error});
    }
}

module.exports.enable = async(req,res)=>{
    try{
        let user = await User.findById(req.params.id);
        if(user.role!='SuperAdmin'){
            await User.findByIdAndUpdate(req.params.id,{activationStatus:true}).select('-salt -password -__v -provider')
            .then(data=>res.send({success:true,data:data}))
            .catch(error=>res.status(400).send({success:false}));
        }
    }catch(error){
        res.status(400).send({error:error});
    }
}


module.exports.disable = async(req,res)=>{
    try{
        let user = await User.findById(req.params.id);
        if(user.role!='SuperAdmin'){
            await User.findByIdAndUpdate(req.params.id,{activationStatus:false}).select('-salt -password -__v -provider')
            .then(data=>res.send({success:true,data:data}))
            .catch(error=>res.status(400).send({success:false}));
        }
    }catch(error){
        res.status(400).send({error:error});
    }
}

module.exports.changePassword = (req, res)=> {
    //getting user id,old password and new password
    var userId = req.user._id;
    var oldPass = String(req.body.oldPassword);
    var newPass = String(req.body.newPassword);
    //getting user data
    return User.findById(userId)
        .exec()
        .then((user) => {
            //checking whether oldpassword value is same or not
            //if it's matched
            if (user.authenticate(oldPass)) {
                //then update this by user requested new password req.params
                user.password = newPass;
                //update those changes to database
                return user
                    .save()
                    .then(() => {
                        return res.status(200).send('Ok');
                    })
                    .catch(validationError(res));
            }
            //if old password value didn't matched by existing password
            //denied user request and send some error message
            else {
                return res.status(403).send("wrong old password");
            }
        });
}

module.exports.updateLoggedUser = async(req,res)=>{
    try{
        await User.findByIdAndUpdate(req.user._id,req.body,{new:true})
        .then(data=>{
            console.log(data);
            res.send({success:true,msg:"update successfully"})
        })
        }
    catch(error){
        res.status(400).send({error:error});
    }
}

module.exports.updateLoggedUser = async(req,res)=>{
    try{
        await User.findByIdAndUpdate(req.user._id,req.body,{new:true})
        .then(data=>{
            console.log(data);
            res.send({success:true,msg:"update successfully"})
        })
        }
    catch(error){
        res.status(400).send({error:error});
    }
}

module.exports.forgotPasswordOtp = async(req,res)=>{
    let otp = Math.floor((Math.random() * 1000000) + 54);
    let user = await User.findOneAndUpdate({email:req.body.email},{otp:otp,otpTime:new Date()},{new:true});
    if(!user){
        return res.status(404).send({success:false,message:"user not available"});
    }
    await sendEmail(req.body.email,otp).then(result=>{
        return res.send({success:true,message:"send successfully"})
    }).catch(error=>{
            console.log(error)
            res.status(400).send({success:false,error:error})
        
    })
    
    
}
module.exports.resetPassword = async(req,res)=>{
    let pendingUser = await User.findOne({email:req.body.email});
    let otpTime = pendingUser.otpTime;
    let expand = moment(otpTime).add(20,'m').toDate();
    if(expand<new Date()){
        return res.status(404).send({success:false,message:"otp time expired"})
    }

    if(String(pendingUser.otp) != String(req.body.otp)){
        return res.status(402).send({success:false,message:"otp is wrong"})
    }
    console.log(pendingUser)
    pendingUser.password = req.body.password;
    await pendingUser.save()
    .then(result=>{
        res.send({sucess:true,message:"sucessfully updated"})
    }).catch(error=>{
        res.status(400).send({success:false,error:error})
    })

}