const router = require('express').Router();
const controller = require('./user.controller');
const auth = require('../auth/auth.controller');

router.post('/',controller.createUser);
//get all user
router.get('/getAll',controller.getAllUser);

// router.patch('/verify/:id',controller.verifiedUser)
//update user
router.put('/update/:id',auth.hasRole('Admin'),controller.updateUser);
router.get('/get',auth.isAuthenticated(),controller.getLoggedUser);
router.delete('/delete/:id',auth.hasRole('Admin'),controller.deleteUser);
router.patch('/enable/:id',auth.hasRole('Admin'),controller.enable);
router.patch('/disable/:id',auth.hasRole('Admin'),controller.disable);
router.post('/updatePassword/:id',auth.hasRole('Admin'),controller.updatePassword);
router.post('/changePassword',auth.isAuthenticated(),controller.changePassword);
router.patch('/update',auth.isAuthenticated(),controller.updateLoggedUser);
// router.post('/forgotPassword',controller.forgotPasswordOtp);
// router.post('/resetPassword',controller.resetPassword)
router.get('/getStudent',controller.getStudents);
router.delete('/deleteStudent',controller.deleteStudent);
module.exports = router;