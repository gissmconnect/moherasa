'use strict';

const { Schema } =  require('mongoose');
const mongoose = require('mongoose');
const User = require('./user.model');

const EditorSchema = new Schema(
    {
        
    },
    {
        discriminatorKey: 'role'
    }
);


module.exports = User.discriminator('Agent', EditorSchema);