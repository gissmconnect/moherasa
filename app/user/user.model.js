'use strict';
/*eslint no-invalid-this:0*/
const crypto = require('crypto');
const mongoose = require('mongoose');
const { isDate } = require('util');
const Schema = mongoose.Schema;

// mongoose.Promise = require('bluebird');

const authTypes = ['github', 'twitter', 'facebook', 'google'];

var BaseUserSchema = new Schema({
    verificationStatus: {
        type: Boolean,
        default: true,
    },
    name: {
        type: String,
        default: ''
    },
    facebookId: {
        type: Number,
    },
    googleId: {
        type: Number,
    },
    imageId: {
        type: String,
    },
    imageUrl: {
        type: String,
    },
    rememberMe: {
        type: Boolean,
        default: false,
    },
    countryCode: {
        type: Number,
    },
    contactPerson: {
        type: String,
    },
    verificationCode: {
        type: Number,
    },

    phoneNumber: {
        type: String,
    },
    contactNumber: {
        type: Number,
    },

    city: {
        type: String,
    },
    //activated
    activationStatus: {
        type: Boolean,
        default: true,
    },
    state: {
        type: String,
    },
    country: {
        type: String,
    },
    zip: {
        type: Number,
    },
    currency: {
        type: String,
    },
    aboutUs: {
        type: String,
    },
    logo: {
        type: String,
    },
    publicId: {
        type: String,
    },
    userImage: {
        type: String,
    },
    userImagePublicId: {
        type: String,
    },
    //for staff
    status: {
        type: Boolean,
        default:true,
    },
    //*********************for card details stripe payment card

    createdAt: {
        type: Date,
        default: Date.now,
    },
    updatedAt: {
        type: Date,
    },
    forgetPasswordNo: {
        type: Number,
    },
    userStatuActivation: {
        type: String,
    },
    device_token: {
        type: String,
        default: ''
    },
    otp:{
        type:Number
    },
    otpTime:{
        type:Date
    },
    username: {
        type: String,
        required: true,
        index: true,
        unique: true
    },
    email: {
        type: String,
        lowercase: true,
        unique:true,
        required:true
    },
    role: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required() {
            if (authTypes.indexOf(this.provider) === -1) {
                return true;
            } else {
                return false;
            }
        },
    },
    userType:{
        type:String,
        enum:['ONA Team','Freelancer']
    },
    provider: String,
    salt: String,
    facebook: {},
    twitter: {},
    google: {},
    github: {},
},
    {
        discriminatorKey: 'role',
        timestamps: true
    }
);

/**
 * Virtuals
 */

// Public profile information
BaseUserSchema.virtual('profile').get(function () {
    return {
        name: this.name,
        role: this.role,
    };
});

// Non-sensitive info we'll be putting in the token
BaseUserSchema.virtual('token').get(function () {
    return {
        _id: this._id,
        role: this.role,
    };
});

/**
 * Validations
 */


// Validate empty password
BaseUserSchema.path('password').validate(function (password) {
    if (authTypes.indexOf(this.provider) !== -1) {
        return true;
    }
    return password.length;
}, 'Password cannot be blank');

var validatePresenceOf = function (value) {
    return value && value.length;
};

/**
 * Pre-save hook
 */
BaseUserSchema.pre('save', function (next) {
    // Handle new/update passwords
    if (!this.isModified('password')) {
        return next();
    }

    if (!validatePresenceOf(this.password)) {
        if (authTypes.indexOf(this.provider) === -1) {
            return next(new Error('Invalid password'));
        } else {
            return next();
        }
    }

    // Make salt with a callback
    this.makeSalt((saltErr, salt) => {
        if (saltErr) {
            return next(saltErr);
        }
        this.salt = salt;
        this.encryptPassword(this.password, (encryptErr, hashedPassword) => {
            if (encryptErr) {
                return next(encryptErr);
            }
            this.password = hashedPassword;
            return next();
        });
    });
});

/**
 * Methods
 */
BaseUserSchema.methods = {
    /**
     * Authenticate - check if the passwords are the same
     *
     * @param {String} password
     * @param {Function} callback
     * @return {Boolean}
     * @api public
     */
    authenticate(password, callback) {
        if (!callback) {
            return this.password === this.encryptPassword(password);
        }

        this.encryptPassword(password, (err, pwdGen) => {
            if (err) {
                return callback(err);
            }

            if (this.password === pwdGen) {
                return callback(null, true);
            } else {
                return callback(null, false);
            }
        });
    },

    /**
     * Make salt
     *
     * @param {Number} [byteSize] - Optional salt byte size, default to 16
     * @param {Function} callback
     * @return {String}
     * @api public
     */
    makeSalt(byteSize, callback) {
        var defaultByteSize = 16;

        if (typeof arguments[0] === 'function') {
            callback = arguments[0];
            byteSize = defaultByteSize;
        } else if (typeof arguments[1] === 'function') {
            callback = arguments[1];
        } else {
            throw new Error('Missing Callback');
        }

        if (!byteSize) {
            byteSize = defaultByteSize;
        }

        return crypto.randomBytes(byteSize, (err, salt) => {
            if (err) {
                return callback(err);
            } else {
                return callback(null, salt.toString('base64'));
            }
        });
    },

    /**
     * Encrypt password
     *
     * @param {String} password
     * @param {Function} callback
     * @return {String}
     * @api public
     */
    encryptPassword(password, callback) {
        if (!password || !this.salt) {
            if (!callback) {
                return null;
            } else {
                return callback('Missing password or salt');
            }
        }

        var defaultIterations = 10000;
        var defaultKeyLength = 64;
        var salt = new Buffer(this.salt, 'base64');

        if (!callback) {
            return crypto
                .pbkdf2Sync(
                    password,
                    salt,
                    defaultIterations,
                    defaultKeyLength,
                    'sha1'
                )
                .toString('base64');
        }

        return crypto.pbkdf2(
            password,
            salt,
            defaultIterations,
            defaultKeyLength,
            'sha1',
            (err, key) => {
                if (err) {
                    return callback(err);
                } else {
                    return callback(null, key.toString('base64'));
                }
            }
        );
    },
};



const User = mongoose.model('User', BaseUserSchema);

module.exports = User;
