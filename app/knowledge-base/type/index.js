const router = require('express').Router();
const controller = require('./articleType.controller');

router.post('/',controller.create);

router.get('/',controller.get);

router.put('/:id',controller.update);

router.delete('/:id',controller.delete);

module.exports = router;

