const mongoose = require('mongoose');

const Schema = new mongoose.Schema({
    type:String,
    typeAr:String,
    isActive:{
        type:Boolean,
        default:true
    }
},{
    timestamps:true
});

const type = mongoose.model('Type',Schema);

module.exports = type;