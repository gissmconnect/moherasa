const Type = require('./articleType.model');

module.exports.create = async(req,res)=>{
    let type = new Type(req.body);
    await type.save().then(data=>res.send({success:true,data:data}))
    .catch(err => res.status(400).send({success:false,error:err}));
}

module.exports.get = async(req,res)=>{
    let type = await Type.find({isActive:true}).sort({createdAt:-1});
    if(type){
        return res.send({success:true,data:type});
    }
    return res.status(404).send({error:"No data found"})
}

module.exports.update = async(req,res)=>{
    await Type.findByIdAndUpdate(req.params.id,req.body,{
        upsert:true
    }).then(data=>res.send({success:true}))
    .catch(error=> res.status(400).send(error));
}

module.exports.delete = async(req,res)=>{
    await Type.findByIdAndUpdate(req.params.id,{isActive:false},{new:true})
    .then(data=>{
        return res.send({success:true,msg:"delete successfully"})
    })
    .catch(err=>res.status(400).send({success:false,error:err}));
}