const mongoose = require('mongoose');

const Schema = mongoose.Schema({
    kb_title: { type: mongoose.Types.ObjectId,ref:'Type' },
    kb_body: { type: 'string' },
    kb_permalink: { type: 'string' },
    kb_published: { type: 'boolean' },
    kb_keywords: { type: 'string' },
    kb_author_email: { type: 'string' },
    // kb_password: { type: 'string' },
    kb_featured: { type: 'boolean' },
    kb_seo_title: { type: 'string' },
    kb_seo_description: { type: 'string' }
},{
    timestamps:true
})

module.exports = mongoose.model('KnowledgeBase',Schema);