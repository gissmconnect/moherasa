const router = require('express').Router();
const controller = require('./knowledge.controller');
const auth = require('../../auth/auth.controller');
router.post('/insert_kb',auth.isAuthenticated(),controller.create)
router.get('/getAll',controller.getAll);
router.get(['/search/:tag', '/topic/:tag'],controller.searchTopicTag);
module.exports = router;