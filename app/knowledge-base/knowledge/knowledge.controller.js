const Model = require('./knowledge.model');
const User = require('../../user/user.model')
const config = require('../../config/config.json')
const lunr = require('lunr')
const common = require('../../common');

module.exports.create = async(req,res)=>{
    const lunr_index = req.app.index;
    req.body.kb_author_email = req.user.email;
    console.log(lunr_index.invertedIndex)
    new Model(req.body).save()
    .then(newDoc=>{
        let keywords = '';
                    if(req.body.kb_keywords !== undefined){
                        keywords = req.body.kb_keywords.toString().replace(/,/g, ' ');
                    }

                    // get the new ID
                    let newId = newDoc._id;
                    // if(config.settings.database.type !== 'embedded'){
                    //     newId = newDoc.insertedIds;
                    // }

                    // create lunr doc
                    const lunr_doc = {
                        kb_title: req.body.kb_title,
                        kb_keywords: keywords,
                        id: newId
                    };

                    console.log('lunr_doc', lunr_doc);

                    // if index body is switched on
                    if(config.settings.index_article_body === true){
                        lunr_doc['kb_body'] = req.body.kb_body;
                    }

                    // add to lunr index
                    // console.log(lunr_index)
                    lunr(function(){
                        this.field('kb_title');
                        this.field('kb_keywords');
                        this.ref('id');

                    // add body to this if in config
                    if(config.settings.index_article_body === true){
                        this.field('kb_body');
                    }
                    this.add(lunr_doc)
                    });
                    res.send(lunr_doc)
    }).catch(error=>{
        res.send({success:false,error:error})
    })
}

module.exports.getAll = async(req,res)=>{
    try{
        let allKnowledge = await Model.find();
        res.send({success:true,data:allKnowledge})
    }catch(error){
        res.status(400).send({success:false,error:error})
    }
}

// search kb's
 module.exports.searchTopicTag = async(req, res) => {
    const search_term = req.params.tag;
    const lunr_index = req.app.index;

    // determine whether its a search or a topic
    let routeType = 'search';
    if(req.path.split('/')[1] === 'topic'){
        routeType = 'topic';
    }

    // we strip the ID's from the lunr index search
    const lunr_id_array = [];
    lunr_index.search(search_term).forEach((id) => {
        // if mongoDB we use ObjectID's, else normal string ID's
        if(config.settings.database.type !== 'embedded'){
            lunr_id_array.push(common.getId(id.ref));
        }else{
            lunr_id_array.push(id.ref);
        }
    });

    const featuredCount = config.settings.featured_articles_count ? config.settings.featured_articles_count : 4;

    // get sortBy from config, set to 'kb_viewcount' if nothing found
    // const sortByField = typeof config.settings.sort_by.field !== 'undefined' ? config.settings.sort_by.field : 'kb_viewcount';
    // const sortByOrder = typeof config.settings.sort_by.order !== 'undefined' ? config.settings.sort_by.order : -1;
    // const sortBy = {};
    // sortBy[sortByField] = sortByOrder;

    // we search on the lunr indexes
    let data = await Model.find({ _id: { $in: lunr_id_array }, kb_published: 'true',/* kb_versioned_doc: { $ne: true },*/}, (err, results) => {
    res.send({success:true,data:data})      
    });
}