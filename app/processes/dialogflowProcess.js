const rp = require('request-promise');
const ip = require('ip');
const saveMessageQueue = require('../queues/saveMessage');
const axios = require('axios');
require('dotenv').config();
function checkEnglish(word){
        var english = /^[A-Za-z0-9 ]*$/;
        if (english.test(word)){
                return true
        }
        else{
                return false
        }

}
module.exports = async function (job, done) {
    try {
        //get query from 360 dialog as a data and call api for getting response from dialogflow
        let data = job.data.response;
	    let reply = 0;
	    if(reply==0){
		    reply = data.messages[0].text.body
	    }
        let response = await rp.post(`${process.env.BASE_RASA}/webhooks/rest/webhook`,{
            json:{
		        sender:data.messages[0].from,
                message:reply,
                input_channel: "whatsapp"
            },
		    headers:{"Content-Type": "application/json"}
        })
        // console.log("response",response.reply,data.messages[0].from,"message")
        //return back to 360-dialog
        //check greeting message in english
        let send;
        if(response[0].custom){
            if(response[0].custom.pdf){
                send = await initPdf(response[0].custom.pdf,data.messages[0].from,"chatbot");
            }
        }
        else{
            send = await init(response[0].text,data.messages[0].from,"chatbot");
        }
        //code
        //now save the query and response
        let Model = {
            from:data.messages[0].text.from,
            id:data.messages[0].id,
            timestamp:data.messages[0].timestamp,
            type:data.messages[0].type,
            text:data.messages[0].text.body,
            reply:response.text,
	        send_id:send.messages[0].id
        }
        await saveMessageQueue.add({
            Model:Model
        },{
            attempts: 2,
            removeOnFail:true,
            removeOnComplete:true
        })
        done();
    }catch(error){
        console.log(error);
        done();
    }
}

async function init(reply,to,intent){
    let verifyContactUrl = `https://waba.360dialog.io/v1/contacts`;
    const contacts = ['+'+to];
    console.log(contacts,"************to***********",reply,"reply")
    try {
        const response = await axios.post(verifyContactUrl, {
            blocking: "wait",
            contacts: contacts
        }, {
            headers: {
                'D360-API-KEY': process.env.key_360
            }
        });
        const data = response.data;
        let contactData = data.contacts[0];
        console.log(contactData)
        if(contactData.status === 'valid'){
            let contactId = contactData.wa_id;
            console.log(contactId)
            let sendJson = {
                "recipient_type": "individual",
                "to": contactId
            }   
            sendJson.type = "text"
            sendJson.text = {
                "body": reply
            }
            let sendMessageUrl = `https://waba.360dialog.io/v1/messages`;
            const response = await axios.post(sendMessageUrl, sendJson, {
                headers: {
                    'D360-API-KEY': process.env.key_360
                }
            });
            console.log(" **  message sent **** ", response.data);
	        return response.data

        } else {
            console.log(" *** phone number is invalid *** ");
        }

    } catch (err){
        console.log(" ** error while sending sms", err);
    }    
}

// init();

async function initPdf(reply,to,intent){
    let verifyContactUrl = `https://waba.360dialog.io/v1/contacts`;
    const contacts = ['+'+to];
    console.log(contacts,"************to***********",reply,"reply")
    try {
        const response = await axios.post(verifyContactUrl, {
            blocking: "wait",
            contacts: contacts
        }, {
            headers: {
                'D360-API-KEY': process.env.key_360
            }
        });
        const data = response.data;
        let contactData = data.contacts[0];
        console.log(contactData)
        if(contactData.status === 'valid'){
            let contactId = contactData.wa_id;
            console.log(contactId)
            let sendJson = {
                "recipient_type": "individual",
                "to": contactId,
                type:"document",
                document:{
                    caption:"course pdf",
                    link:reply
                }
            }
            //split the whole string int list of word
       
		
            // if(intent == 'bca'){
	        // sendJson.type = "template"
            //     sendJson.template = {
            //         "namespace":"549abefd_05cc_4221_bb30_f9aa514586a3",
		    //         "name": "your_college",                 
		    //         "language": {
      		//             "policy": "deterministic",
            //               "code": "en",
            //         },
            //         //"localizable_params": [ 
            //         //    { "default": "Atul" }
            //         //]
            //          "components":[
            //             {
            //                 "type": "body",
            //                 "parameters": [
            //                     {
            //                         "type": "text",
            //                         "text": "Candidate"
            //                     }
            //                 ]
            //             }
            //          ]

            //     }
            // }
            
            let sendMessageUrl = `https://waba.360dialog.io/v1/messages`;
            const response = await axios.post(sendMessageUrl, sendJson, {
                headers: {
                    'D360-API-KEY': process.env.key_360
                }
            });

            console.log(" **  message sent **** ", response.data);
	    return response.data

        } else {
            console.log(" *** phone number is invalid *** ");
        }

    } catch (err){
        console.log(" ** error while sending sms", err);
    }    
}
