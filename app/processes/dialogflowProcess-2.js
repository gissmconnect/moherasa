const rp = require('request-promise');
const ip = require('ip');
const saveMessageQueue = require('../queues/saveMessage');
const axios = require('axios');
const {requestLogger} = require('../config/logger');
require('dotenv').config();

function checkEnglish(word){
	var english = /^[A-Za-z0-9 ]*$/;
	if (english.test(word)){
   		return true	
	}
	else{
    		return false
	}
	
}
module.exports = async function (job, done) {
    try {
        //get query from 360 dialog as a data and call api for getting response from dialogflow
        let data = job.data.response;
	    let reply = 0;
	    console.log("LLLLLLLLLLLLLLLLL",data)
	    if(data.messages[0]){
	    	if(data.messages[0].button){
			console.log(data.messages[0].button,"FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF")
			reply = data.messages[0].button.text
		}
	    }
	if(reply==0){
		reply = data.messages[0].text.body
	}
	console.log("before before before")
        let response = await rp.post(`http://${ip.address()}:3012/api/dialog`,{
            json:{
		from:data.messages[0].from,
                text:reply
            }
        })	
        console.log("response",response.reply,data.messages[0].from,"message")
        //return back to 360-dialog
	    //
	console.log(data.messages,"dta messages *******")
        let send = await init(response.reply,data.messages[0].from,response.intent);
        //code
        //now save the query and response
        let Model = {
            from:data.messages[0].from,
            id:data.messages[0].id,
            timestamp:data.messages[0].timestamp,
            type:data.messages[0].type,
            text:data.messages[0].text.body,
            reply:response.reply,
	    send_id:send.messages[0].id
        }
        await saveMessageQueue.add({
            Model:Model
        },{
            attempts: 2,
            removeOnFail:true,
            removeOnComplete:true
            
        })
	    requestLogger.info(`from : ${Model.from}, query : ${Model.text}, reply : ${Model.reply}, sessionId:${job.data.sessionId}`)

        done();
    }catch(error){
        console.log(error);
        done();
    }
}

async function init(reply,to,intent){
    let verifyContactUrl = `https://waba.360dialog.io/v1/contacts`;
    const contacts = ['+'+to];
    console.log(contacts,"************to***********",reply,"reply")
    try {
        const response = await axios.post(verifyContactUrl, {
            blocking: "wait",
            contacts: contacts
        }, {
            headers: {
                'D360-API-KEY': 'MflMIXdEZmfdgjKYk3pr6aj1AK'
            }
        });
        const data = response.data;
        let contactData = data.contacts[0];
        console.log(contactData)
        if(contactData.status === 'valid'){
            let contactId = contactData.wa_id;
            console.log(contactId)
            let sendJson = {
                "recipient_type": "individual",
                "to": contactId
            }
            //split the whole string int list of word
        const reply_list = reply.split(" ");
        console.log(reply_list,"******reply list******")
        if(reply_list[0]=='template'){
            sendJson.type = "template";
            sendJson.template = {
                "namespace":"549abefd_05cc_4221_bb30_f9aa514586a3",
                "name": reply_list[1],                 
                "language": {
                      "policy": "deterministic",
                      "code": "en",
                },
                components:[{
                    "type":"body",
                    "parameters":[]
                }]
            }
            //first word for template, second for template name 
            for(let i=2;i<reply_list.length;i++){
                sendJson.template.components[0].parameters.push({type:"text",text:reply_list[i]})
            }

        }
            // if(intent == 'bca'){
	        // sendJson.type = "template"
            //     sendJson.template = {
            //         "namespace":"549abefd_05cc_4221_bb30_f9aa514586a3",
		    //         "name": "your_college",                 
		    //         "language": {
      		//             "policy": "deterministic",
            //               "code": "en",
            //         },
            //         //"localizable_params": [ 
            //         //    { "default": "Atul" }
            //         //]
            //          "components":[
            //             {
            //                 "type": "body",
            //                 "parameters": [
            //                     {
            //                         "type": "text",
            //                         "text": "Candidate"
            //                     }
            //                 ]
            //             }
            //          ]

            //     }
            // }
            else
            {
		        sendJson.type = "text"
                sendJson.text = {
                    "body": reply
                }
            }
            let sendMessageUrl = `https://waba.360dialog.io/v1/messages`;
            const response = await axios.post(sendMessageUrl, sendJson, {
                headers: {
                    'D360-API-KEY': process.env.key_360
                }
            });

            console.log(" **  message sent **** ", response.data);
	    return response.data

        } else {
            console.log(" *** phone number is invalid *** ");
        }

    } catch (err){
        console.log(" ** error while sending sms", err);
    }    
}

// init();

