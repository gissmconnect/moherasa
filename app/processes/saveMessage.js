const Model = require('../dialog-flow/model');
module.exports = async function (job, done) {
    try {
        let data = job.data.Model;
        await new Model(data)
        .save()
        .then(data=>{
            console.log(data);
            done();
        })
        .catch(error=>{
            console.log(error);
            done();
        })
    }
    catch(error){
        console.log(error)
        done();
    }
}