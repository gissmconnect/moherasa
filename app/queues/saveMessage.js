require('dotenv').config();
const Queue = require('bull');
const saveMessage = new Queue('save-message-testing', {
    redis: { port: process.env.REDIS_PORT, host: process.env.REDIS_HOST },
});

module.exports = saveMessage;
