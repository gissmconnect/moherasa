require('dotenv').config();
const Queue = require('bull');
const dialogflowQueue = new Queue('dialogflow-assignment-testing', {
    redis: { port: process.env.REDIS_PORT, host: process.env.REDIS_HOST },
});

module.exports = dialogflowQueue;
