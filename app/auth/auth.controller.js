require('dotenv').config();
const jwt = require('jsonwebtoken');
const config = require('../config/config');


const expressJwt = require('express-jwt');
const compose = require('composable-middleware');
const User = require('../user/user.model');

var validateJwt = expressJwt({
    secret: process.env.secret,
    algorithms:["HS256"]
});

/**
 * Attaches the user object to the request if authenticated
 * Otherwise returns 403
 */
function isAuthenticated () {
    return (
        compose()
            // Validate jwt
            .use(function (req, res, next) {
                // allow access_token to be passed through query parameter as well
                if (req.query && req.query.hasOwnProperty('access_token')) {
                    req.headers.authorization = `Bearer ${req.query.access_token}`;
                }
                // IE11 forgets to set Authorization header sometimes. Pull from cookie instead.
                if (
                    req.query &&
                    typeof req.headers.authorization === 'undefined'
                ) {
                    req.headers.authorization = `Bearer ${req.cookies.token}`;
                }
                validateJwt(req, res, next);
            })
            // Attach user to request
            .use(function (req, res, next) {
                console.log(req.user._id,"IIIIIIIIII")
                User.findById(req.user._id)
                    .exec()
                    .then((user) => {
                        if (!user) {
                            return res.status(401).end();
                        }
                        req.user = user;
                        next();
                    })
                    .catch((err) => next(err));
            })
    );
}

/**
 * Checks if the user role meets the minimum requirements of the route
 */
module.exports.hasRole = function(roleRequired) {
    if (!roleRequired) {
        throw new Error('Required role needs to be set');
    }

    return compose()
        .use(isAuthenticated())
        .use(function meetsRequirements(req, res, next) {
            if (
                config.userRoles.indexOf(req.user.role) >=
                config.userRoles.indexOf(roleRequired)
            ) {
                return next();
            } else {
                return res.status(403).send('Forbidden');
            }
        });
}

module.exports.signToken = (id, role) => {
    return jwt.sign({ _id: id, role }, process.env.secret, {
        expiresIn: 60 * 60 * 24 * 30,
    });
}

module.exports.isAuthenticated = isAuthenticated;