const router = require('express').Router();
const User = require('../user/user.model');

// Passport Configuration
require('./local/passport').setup(User);
router.use('/',require('./local'));

module.exports = router;