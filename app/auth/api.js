'use strict';
const express = require('express');
const { signToken } = require('./auth.controller');
const passport = require('passport');
// const elasticSearch = require('../config/elasticsearch');
const { requestLogger } = require('../config/logger');
const {errorLogger} = require('../config/logger');

var router = express.Router();

router.use('/login', function (req, res, next) {
    passport.authenticate('api', async function (err, user, info) {
        var error = err || info;
        if (error) {
            errorLogger.error(error)
            return res.status(401).json(error);
        }
        if (!user) {
            errorLogger.error(`A log generated due to not registered user`)
            return res
                .status(404)
                .json({ message: 'Something went wrong, please try again.' });
        }
        user.device_token = req.body.device_token || "";
        await user.save();

        const userData = {
            _id: user._id,
            email: user.email,
            name: user.name,
            role:user.role,
        };
        var token = signToken(user._id, user.role);
        res.json({ token, data: userData });
    })(req, res, next);
});

module.exports = router;
