const passport = require('passport');
const  {Strategy} = require('passport-local');

let LocalStrategy = Strategy;
function localAuthenticate(User, email, password, done) {
    User.findOne({
        email: email.toLowerCase()
    })
        .exec()
        .then((user) => {
            if (!user) {
                return done(null, false, {
                    message: 'User not registered.',
                });
            }
            user.authenticate(password, function (authError, authenticated) {
                if (authError) {
                    return done(authError);
                }
                if (!authenticated) {
                    return done(null, false, {
                        message: 'This password is not correct.',
                    });
                }
                if (user.activationStatus == false) {
                    return done(null, false, {
                        message: 'Your account has been deactivated.',
                    });
                } else {
                    return done(null, user);
                }
            });
        })
        .catch((err) => done(err));
}

function localAuthenticate1(User, email, password, done) {
    User.findOne({
        email: email.toLowerCase()
    })
        .exec()
        .then((user) => {
            if (!user) {
                return done(null, false, {
                    message: 'User not registered.',
                });
            }
            user.authenticate(password, function (authError, authenticated) {
                if (authError) {
                    return done(authError);
                }
                if (!authenticated) {
                    return done(null, false, {
                        message: 'This password is not correct.',
                    });
                }
                if (user.role != 'Admin') {
                    return done(null, false, {
                        message: 'You are not Not Admin',
                    });
                }
                else {
                    return done(null, user);
                }
            });
        })
        .catch((err) => done(err));
}

module.exports.setup = (User /*, config*/) => {
    passport.use('local',
        new LocalStrategy(
            {
                usernameField: 'email',
                passwordField: 'password', // this is the virtual field on the model
            },
            function (email, password, done) {
                console.log("^^^^^^^^^^^^")
                return localAuthenticate1(User, email, password, done);
            }
        )
    );
    passport.use('api',
        new LocalStrategy(
            {
                usernameField: 'email',
                passwordField: 'password', // this is the virtual field on the model
            },
            function (email, password, done) {

                return localAuthenticate(User, email, password, done);
            }
        )
    );
}