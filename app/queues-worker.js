require('dotenv').config();
require('./config/mongoose-connection');


const dialogflowAssignmentQueue = require('./queues/dialogflowassignment');
const saveMessageQueue = require('./queues/saveMessage');

dialogflowAssignmentQueue.process(require('./processes/dialogflowProcess'));
saveMessageQueue.process(require('./processes/saveMessage'));

console.log("********** queue is running *************");