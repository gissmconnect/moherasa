require('dotenv').config();
const express = require('express');
var ip = require('ip');
var app = require('express')();
var http = require('http').createServer(app);
require('./config/mongoose-connection');
// var io = require('socket.io')(http);
var path = require('path');
var cors = require('cors');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
const fs = require('fs');
const markdownit = require('markdown-it')({ html: true, linkify: true, typographer: true });
const https = require('https');
const config = require('./config/config.json')
const common = require('./common')
const i18n = new (require('i18n-2'))({
    locales: ['en','arabic', 'ar', 'de', 'da', 'es', 'cn', 'ru', 'pt-br', 'jp', 'fi', 'sv', 'tr'],
    directory: path.join(__dirname, 'locales/'),
    defaultLocale: 'en',
    cookieName: 'locale'
});
if(config.settings.locale){
    i18n.setLocale(config.settings.locale);
    i18n.setLocaleFromCookie();
}

const registerRoutes = require('./routes')

// var privateKey = fs.readFileSync('/root/ssl/private.key');
// var certificate = fs.readFileSync('/root/ssl/omannews.crt');

app.use(cors());

app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin','*');
    res.header('Access-Control-Allow-Credentials', 'true');
    res.header(
        'Access-Control-Allow-Headers',
        'Origin, X-Requested-With, Content-Type, Accept'
    );
    next();
});
//build lunr index
common.buildIndex((index)=>{
    app.index = index;
    // console.log(index.search("hello")[0].matchData)
});

//view engine setup
app.set('views', path.join(__dirname, 'views'));
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
registerRoutes(app);

// io.on('connection', function (socket) {
//     console.log('user connected');
//     require('./config/socketio').default(socket);
// });
// var credentials = {key: privateKey, cert: certificate};
// var httpsServer = https.createServer(credentials,app);
http.listen(process.env.http_port, function () {
    console.log(`Server running on ${process.env.http_port}`);
    console.log(`http://${ip.address()}:${process.env.http_port}`);
});

// httpsServer.listen(process.env.https_port,()=>{
//     console.log(`Secure Server running on ${process.env.https_port}`);
//     console.log(`https://${ip.address()}:${process.env.https_port}`);
// })
//seed db setup
// setSeedDb();

module.exports = app;
