module.exports = (app)=>{
    app.use('/api/upload',require('./upload'));
    app.use('/api/dialog',require('./dialog-flow'));
    app.use('/api/user',require('./user'))
    app.use('/api/admin/login',require('./auth'));
    app.use('/api/agent',require('./auth/api'));
    app.use('/api/category',require('./knowledge-base/type'))
    app.use('/api/knowledge',require('./knowledge-base/knowledge'));
}