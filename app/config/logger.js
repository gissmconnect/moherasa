const winston = require('winston');
require('winston-daily-rotate-file');
const path = require('path');

module.exports.requestLogger = winston.createLogger({
    level: 'info',
    format: winston.format.json(),
    transports: [
        new winston.transports.Console({
            format: winston.format.simple()
        }),
        new (winston.transports.DailyRotateFile)({
            filename: path.resolve('logs', 'request-%DATE%.log'),
            maxSize: '20m',
            maxFiles: '14d'
        }),
    ]
});

module.exports.queueLogger = winston.createLogger({
    level: 'info',
    format: winston.format.json(),
    transports: [
        new winston.transports.Console({
            format: winston.format.simple()
        }),
        new (winston.transports.DailyRotateFile)({
            filename: path.resolve('logs', 'queue-%DATE%.log'),
            maxSize: '20m',
            maxFiles: '14d'
        })
    ]
});

module.exports.errorLogger = winston.createLogger({
    level: 'error',
    format: winston.format.json(),
    transports: [
        new winston.transports.Console({
            format: winston.format.simple()
        }),
        new (winston.transports.DailyRotateFile)({
            filename: path.resolve('logs', 'error-%DATE%.log'),
            maxSize: '20m',
            maxFiles: '14d'
        })
    ]
});
