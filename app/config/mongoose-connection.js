require('dotenv').config();
const mongoose = require('mongoose');
// Connect to MongoDB
console.log(process.env.db_uri)
mongoose.connect(process.env.db_uri, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true,
});

mongoose.connection.on('error', function (err) {
    console.error(`MongoDB connection error: ${err}`);
    process.exit(-1);
});

module.exports = mongoose.connection;
