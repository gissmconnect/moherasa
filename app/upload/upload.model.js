const mongoose = require('mongoose');

const Schema = mongoose.Schema({
    url:String,
    secureUrl:String,

},{
    timestamps:true
});

module.exports = mongoose.model('File',Schema);