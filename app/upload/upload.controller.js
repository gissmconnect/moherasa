const config = require('../developementConfig/config');
var multiparty = require('multiparty');
const cloudinary = require('cloudinary').v2;
const Model = require('./upload.model');
cloudinary.config(config.cloudinaryConfig);

function getFormMultiparty(req) {
    return new Promise(function (resolve, reject) {
        const form = new multiparty.Form();
        form.parse(req, function (err, fields, files) {
            // console.log(" parsed body ", err, files,fields )
            if (err) {
                reject(err);
            }
            resolve({ files, fields });
        });
    });
}

module.exports.create = async(req,res)=>{
    const { files } = await getFormMultiparty(req);
    console.log()
    cloudinary.uploader.upload(files.file[0].path, function (err,result) {
        if (err) {
            console.log(err)
            res.status(400).send({ message: 'some thing err to upload' });
        } else {
            var data = {};
            data.mainUrl = result.url;
            console.log(" ****** result data ****** ", result);
            res.send(data);
            new Model({
                url:result.url,
                secureUrl:result.secure_url
            }).save();
        }
    });
}