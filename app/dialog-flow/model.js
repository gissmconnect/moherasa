const mongoose = require('mongoose');

const Schema = mongoose.Schema({
    from:String,
    id:String,
    timestamp:String,
    type:String,
    text:String,
    reply:String,
    send_id:String,
    pricing:Object
},{
    timestamps:true
})

Schema.index({"from":1});

module.exports = mongoose.model('MessageLogs',Schema);
