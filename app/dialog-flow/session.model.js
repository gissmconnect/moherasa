const mongoose = require('mongoose');

const Schema = new mongoose.Schema({
    phoneNumber:{
        type:String
    },
    sessionId:{
        type:String
    }
},{
	timestamps:true
})

module.exports= mongoose.model('SessionModel',Schema)
