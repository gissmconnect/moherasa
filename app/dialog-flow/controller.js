const dialogflow = require('@google-cloud/dialogflow');
const uuid = require('uuid');
require('dotenv').config();
let projectId = process.env.project_id;
const dialogflowQueue = require('../queues/dialogflowassignment');
const Model = require('./model')
const sessionModel = require('./session.model');
const rp = require('request-promise')
const {requestLogger} = require('../config/logger');
const { count } = require('../queues/dialogflowassignment');


// Create a new session
const sessionClient = new dialogflow.SessionsClient();
//const sessionId = uuid.v4();
//const sessionPath = sessionClient.projectAgentSessionPath(projectId, sessionId);

module.exports.getIntent = async(req,res)=>{
    //const sessionId = uuid.v4();
    var now = new Date();
    let session = await sessionModel.findOne({phoneNumber:req.body.from});
    console.log(session,"*****session*****")    
    let sessionPath;
    if(!session){
    	const sessionId = uuid.v4();
	sessionPath = sessionClient.projectAgentSessionPath(projectId, sessionId);
	await new sessionModel({phoneNumber:req.body.from,sessionId:sessionId}).save();

    }
    else{
	let sessionTime = session.updatedAt;
	sessionTime.setMinutes(sessionTime.getMinutes() + 30);
	sessionTime = new Date(sessionTime);
	if(new Date() > sessionTime){
	   const sessionId = uuid.v4();
           sessionPath = sessionClient.projectAgentSessionPath(projectId, sessionId);
	   await sessionModel.findOneAndUpdate({phoneNumber:session.phoneNumber},{sessionId:sessionId},{new:true});

	}
	else{
            sessionPath = sessionClient.projectAgentSessionPath(projectId, session.sessionId);
	}
    }
   
   // The text query request.
   const request = {
    session: sessionPath,
    queryInput: {
      text: {
        // The query to send to the dialogflow agent
        text: req.body.text,
        // The language used by the client (en-US)
        languageCode: 'en-US',
      },
    },
  };

  // Send request and log result
  const responses = await sessionClient.detectIntent(request);
  console.log('Detected intent');
  const result = responses[0].queryResult;
  console.log(`  Query: ${result.queryText}`);
  res.send({query:result.queryText,reply:result.fulfillmentText,intent:result.intent.displayName})
}

module.exports.getMessage = async(req,res)=>{
	var getIP = require('ipware')().get_ip;
	console.log(getIP(req),"body bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb")
  let response = req.body;
  if(response.statuses){
	if(response.statuses.status=="failed"){
	  return res.status(404).send({message:"failed"})
	}
	else{
		let update_logs = await Model.findOneAndUpdate({send_id:response.statuses[0].id},{pricing:response.statuses[0].pricing},{new:true})
		return res.send({success:true,"message":"response has been sended",logs:update_logs});
	}
    
  }
	console.log("*******************after************************")
  await dialogflowQueue.add({
    response:response,
    //sessionId:sessionId
  },{
      attempts: 2,
      removeOnFail:true,
      removeOnComplete:true
  })
  res.send({success:true})
}

module.exports.getWebMessage = async(req,res)=>{
	  if(req.body.auth != process.env.auth){
      return res.status(401).send({success:false,message:"You are not authorized to access this service"})
    }
    let sender = req.body.sender;
    let message = req.body.message;
    let queryValue = req.body.queryValue
    console.log(queryValue,"***query value***",req.body)
    requestLogger.info(`***senderId******${sender}**********`)  
		let response = await rp.post(`${process.env.BASE_RASA}/webhooks/rest/webhook`,{
      json:{
          sender:sender,
          message:message,
          input_channel: "web"
      },
    headers:{"Content-Type": "application/json"}
    })

    if(response.length==0){
      requestLogger.info(`There is connection issue with rasa`);
      return res.status(400).send({success:true,"message":"Sorry, try to next time"});
    }
    let fullResponse = "";
    // console.log(response,"rasa end")
    if(response.length>1){
      console.log("**********")
      if(response[1].custom){
        if(response[1].custom.handover){
        requestLogger.info(`Chat Transfer to agent`);
        let response_mohe = await rp.post(`${process.env.MOHE_API_URL}/api/chat/userRegister`,{
          json:{
              username:sender,
          },
        headers:{"Content-Type": "application/json"}
        })
        console.log(response_mohe,"*********")
        return res.send(response_mohe);
      }
      }
    }
    if(response[0].custom){
      if(response[0].custom.pdf){
        return res.send({success:true,"message":response[0].custom.pdf});      
      }
    }
    
    for(let message of response){
      console.log("message")
      fullResponse += message.text + "\n"
    }

    await new Model({
      from:sender,
      text:queryValue,
      reply:fullResponse
    }).save();
    
		return res.send({success:true,"message":fullResponse,length:response.length});
}




module.exports.resumeRasaBot = async(req,res)=>{
  let sender = req.body.sender;
  let url = `${process.env.BASE_RASA}/conversations/${sender}/tracker/events`;
  requestLogger.info(`****url****${url}`);
  let response_mohe = await rp.post(url,{
    json:{
      "event": "resume"
    },
  headers:{"Content-Type": "application/json"}
  })
  if(response_mohe.sender_id==sender){
    return res.send({success:true,message:"successfully resumed"});
  }

  return res.status(400).send({success:false,message:"not success"});
}

module.exports.report = async(req,res)=>{
  
  let response = {success:true};
    let top_queries = await Model.aggregate([
      {$match:{text:{"$nin":["أهلا","hi","Hi",false,null]}}},
      { $group: { _id: '$text', count: {$sum: 1} } },
      {
        $sort:{
        count:-1
      },
      
    },
    {
      $limit:10
    },
    {
      $project:{
        _id:0,
        question:"$_id",
        count:1
      }
    }
    ])
    response.topQueries = top_queries;
  //total user
  // if(req.query.alluser){
  //   let total = await Model.distinct('from').countDocuments();
  //   response.alluser = total
  // }
  let recentlyAsked = await Model.aggregate([
    {
      $match:{text:{"$nin":["أهلا","hi",null,false,undefined]}}
    },
    {
      "$group":{_id:"$text",createdAt:{"$push":"$createdAt"}}
    },
    {
      "$sort":{
        createdAt:-1
      }
    },
    {
      $limit:10
    },
    {
      $project:{
        _id:0,
        question:"$_id"
      }
    }
  ]) 

  response.recentlyAsked = recentlyAsked;
  
  return  res.send(response)
}

module.exports.mixReport = async(req,res)=>{
  let response = {success:true};
  // let totalUsers = await Model.aggregate([{
  //   $group:{
  //     _id:"$from"
  //   }
  // }])
  let totalUsers = await Model.distinct('from');
  let uniqueQuestion = await Model.aggregate([
    {
      $match:{text:{"$nin":["أهلا","hi","exit","Hi","",null,undefined,false]}}
    },
    {
      "$group":{_id:"$text",count:{$sum:1}},
    },
  ])
  let totalQuestionAsked = await Model.find().countDocuments();

  response.totalUniqueQuestion = uniqueQuestion.length;
  response.total_users = totalUsers.length
  response.total_question = totalQuestionAsked
  res.send(response)
}

module.exports.dayWiseReport = async(req,res)=>{
  let date = req.query.from;
  let report = await Model.aggregate([{
    $match:{
      createdAt:{$gte:new Date(date)}}},
      { $group: {
        _id: {
          $add: [
           { $dayOfYear: "$createdAt"}, 
           { $multiply: 
             [400, {$year: "$createdAt"}]
           }
        ]},   
        queries: { $sum: 1 },
        first: {$min: "$createdAt"}
      }
    },
    { $sort: {_id: 1} },
    { $limit: 30 },
    { $project: { date: "$first", queries: 1, _id: 0} }
  ])
  return res.send(report)
}
