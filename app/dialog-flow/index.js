const router = require('express').Router();
const ipfilter = require('express-ipfilter').IpFilter
const controller = require('./controller')
router.post('/',controller.getIntent);
var ips = ['84.16.243.13','84.16.232.67','84.16.232.77','84.16.232.79','84.16.232.81','84.16.232.83','84.16.232.85','84.16.232.87']
var myips = ['::ffff:35.234.98.51','::ffff:35.242.238.177','::ffff:34.107.83.191','::ffff:106.215.190.7','::ffff:106.215.190.7','::ffff:35.198.146.146','::ffff:35.246.191.205','::ffff:127.0.0.1','::1','::ffff:10.200.2.1']
router.post('/getNotify',ipfilter(myips,{mode:'allow'}),controller.getMessage);

router.post('/getWebMessage',controller.getWebMessage);

router.post('/resume/chat',controller.resumeRasaBot);

router.get('/report',controller.report);
router.get('/mixreport',controller.mixReport);
router.get('/daywise',controller.dayWiseReport)

module.exports = router;
